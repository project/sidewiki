<?php
?>
<div id="sidewiki-main">
<?php if (!empty($entries->entry)): ?>
<?php foreach($entries->entry as $e): ?>
<h4 id="sidewiki-title">
  <a href="<?php echo $e->link[0]->attributes()->href ?>">
    <?php echo $e->title; ?>
  </a>
</h4>
<span id="sidewiki-content">
  <?php echo $e->content ?>
</span>
<?php endforeach;?>
<?php else: ?>
<h4 id="sidewiki-nocontent">
  <?php echo t('No Sidewiki entries for this page.'); ?>
</h4>
<?php endif;?>
</div>
